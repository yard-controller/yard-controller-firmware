/******************************************************************************
 *
 *   Copyright (C) 2023 by Andreas Schmidt. All rights reserved.
 *
 *   SPDX-License-Identifier: GPL-2.0-only
 *
 *****************************************************************************/

#include <linux/module.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/input.h>
#include <linux/init.h>


static char *devicename = "rotary@";
module_param(devicename, charp, 0);
MODULE_PARM_DESC(devicename, "name of rotary input device");

static int reltype = REL_HWHEEL; // 0x06
module_param(reltype, int, 0);
MODULE_PARM_DESC(reltype, "type of relative event to listen for");

static int count_per_press = 1;
module_param(count_per_press, int, 0);
MODULE_PARM_DESC(count_per_press, "event count before a press is generated");

static struct input_dev *button_dev;

static void send_key(int key) {
	input_report_key(button_dev, key, 1);
	input_sync(button_dev);
	input_report_key(button_dev, key, 0);
	input_sync(button_dev);
}

int count = 0;

static void rotary_event(struct input_handle *handle, unsigned int type, unsigned int code, int value)
{
	pr_debug("Event. Dev: %s, Type: %d, Code: %d, Value: %d\n", dev_name(&handle->dev->dev), type, code, value);
	if (type == EV_REL) {
		if (code == reltype) {
			int i;
			int inc = (value > 0) ? 1 : -1;
			if ((inc > 0 && count < 0) || (inc < 0 && count > 0)) { // if change of direction reset count
				count = 0;
			}
			for (i=0; i!=value; i+=inc) {
				count += inc;
				if (abs(count) >= count_per_press) {
					send_key( (inc > 0) ? KEY_UP : KEY_DOWN);
					count = 0;
				}
			}
		}
	}
}

static int rotary_connect(struct input_handler *handler, struct input_dev *dev, const struct input_device_id *id)
{
	struct input_handle *handle;
	int error;

	handle = kzalloc(sizeof(struct input_handle), GFP_KERNEL);
	if (!handle)
		return -ENOMEM;

	handle->dev = dev;
	handle->handler = handler;
	handle->name = "rotary_updownkeys_handle";

	error = input_register_handle(handle);
	if (error)
		goto err_free_handle;

	error = input_open_device(handle);
	if (error)
		goto err_unregister_handle;

	pr_info("Connected device: %s (%s at %s)\n",
			dev_name(&dev->dev),
			dev->name ?: "unknown",
			dev->phys ?: "unknown");

	return 0;

 err_unregister_handle:
	input_unregister_handle(handle);
 err_free_handle:
	kfree(handle);
	return error;
}

bool starts_with(const char *pre, const char *str) {
	size_t lenpre = strlen(pre),
		   lenstr = strlen(str);
	return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
}

// keep record of match and remove record is disconnect
bool matched = false;

static bool rotary_match(struct input_handler *handler, struct input_dev *dev) {
	if (matched)
		return false;
	matched = starts_with(devicename, dev->name);
	return matched;
}

static void rotary_disconnect(struct input_handle *handle) {
	pr_info("Disconnected device: %s\n", dev_name(&handle->dev->dev));

	input_close_device(handle);
	input_unregister_handle(handle);
	kfree(handle);
	matched = false;
}

static const struct input_device_id rotary_ids[] = {
	{ .driver_info = 1 },	/* Matches all devices */
	{ },			/* Terminating zero entry */
};

MODULE_DEVICE_TABLE(input, rotary_ids);

static struct input_handler rotary_handler = {
	.event =	rotary_event,
	.match =	rotary_match,
	.connect =	rotary_connect,
	.disconnect =	rotary_disconnect,
	.name =		"rotary_updownkeys",
	.id_table =	rotary_ids,
};

static int __init button_init(void)
{
	int error;

	button_dev = input_allocate_device();
	if (!button_dev) {
		pr_err("Not enough memory\n");
		error = -ENOMEM;
		return error;
	}

	button_dev->name = "Rotary UpDown Keys";
	button_dev->evbit[0] = BIT_MASK(EV_KEY);// | BIT_MASK(EV_REP);
	set_bit(KEY_DOWN, button_dev->keybit);
	set_bit(KEY_UP, button_dev->keybit);

	error = input_register_device(button_dev);
	if (error) {
		pr_err("Failed to register device\n");
		goto err_free_dev;
	}

	return 0;
err_free_dev:
	input_free_device(button_dev);
	return error;
}

static int __init rotary_updownkeys_init(void)
{
	int error = button_init();

	pr_info("Loading rotary updown keys module...\n");

	if (count_per_press < 1)
		count_per_press = 1;

	if (!error) {
		if (input_register_handler(&rotary_handler)==0) {
			pr_info("Rotary encoder up down keys module loaded.\n");
			return 0;
		} else {
			input_unregister_device(button_dev);
			input_free_device(button_dev);
		}
	}

	return 0;
}

static void __exit rotary_updownkeys_exit(void)
{
	input_unregister_device(button_dev);
	input_free_device(button_dev);
	input_unregister_handler(&rotary_handler);
}

module_init(rotary_updownkeys_init);
module_exit(rotary_updownkeys_exit);

MODULE_AUTHOR("Andreas Schmidt");
MODULE_DESCRIPTION("rotary encoder as up down keys module");
MODULE_ALIAS("rotary-updownkeys");
MODULE_LICENSE("GPL");
