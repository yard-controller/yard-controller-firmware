SUMMARY = "Garden controll image based on core-image-minimal"

COMPATIBLE_MACHINE = "^rpi$"

CORE_IMAGE_EXTRA_INSTALL:append = " \
    kernel-modules \
    rotary-updown-keys \
    linux-firmware-bcm43430 \
    i2c-tools \
    wpa-supplicant \
    openssh \
    opkg \
    \
    ttf-dejavu-sans \
    ttf-dejavu-sans-mono \
    ttf-dejavu-sans-condensed \
    ttf-dejavu-serif \
    ttf-dejavu-serif-condensed \
    ttf-dejavu-common \
    \
    evtest \
    \
    yardctl-start-service \
"

QT6_IMAGE_INSTALL = " \
    qtbase \
"

IMAGE_INSTALL = " \
    packagegroup-core-boot \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    ${QT6_IMAGE_INSTALL} \
"

IMAGE_FEATURES += " \
"

IMAGE_FSTYPES += " \
    wic.bmap \
"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE:append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "", d)}"
