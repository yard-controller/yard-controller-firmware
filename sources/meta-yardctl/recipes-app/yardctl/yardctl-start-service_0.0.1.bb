SUMMARY = "Start yardctl application"
LICENSE = "CLOSED"

SERVICE_NAME = "yardctl-start"

SRC_URI = " \
    file://yardctl-start.service \
"

S = "${WORKDIR}"

inherit systemd allarch

RDEPENDS:${PN} += " \
    systemd \
    yardctl-app \
"

FILES:${PN} += " \
    ${systemd_unitdir}/system \
"

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "yardctl-start.service"

do_install:append() {
    install -d ${D}/${systemd_unitdir}/system
    install -m 644 ${WORKDIR}/yardctl-start.service ${D}/${systemd_unitdir}/system/
}
