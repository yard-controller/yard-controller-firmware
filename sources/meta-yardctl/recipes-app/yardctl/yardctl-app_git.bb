DESCRIPTION = "Yard Controll display application"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=57d76440fc5c9183c79d1747d18d2410"

SRC_URI = "git://gitlab.com/yard-controller/yardctl;protocol=https;branch=main"
SRCREV = "${AUTOREV}"

inherit qt6-cmake
include recipes-qt6/qt6/qt6.inc

S = "${WORKDIR}/git"

DEPENDS += " \
    qtbase \
    qtdeclarative \
    qtdeclarative-native \
"

PACKAGES = " \
    ${PN} \
    ${PN}-dbg \
"

FILES:${PN} += " \
    yardctl \
"

FILES:${PN}-dbt += " \
    /usr/bin/.debug \
    /usr/bin/.debug/yardctl \
"
